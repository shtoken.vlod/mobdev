//
//  SecondViewCONTROLLER.swift
//  UIViewController
//

import Foundation
import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSettings()
    }
    
    func buttonSettings(){
        backButton.layer.cornerRadius = 10
        backButton.backgroundColor = .systemCyan
    }
    
    deinit {
        
    }
    
    @IBAction func didPressedButton(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
}
